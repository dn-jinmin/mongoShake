/**
 * @Author: liaoPengLiang
 * @File:  receiver
 * @Version: 1.0.0
 * @Date: 2023/3/24
 * @Description:
 */

package tunnelx

type Configuration struct {
	Tunnel        string
	TunnelAddress string
	ReplayerNum   int
}
