/**
 * @Author: liaoPengLiang
 * @File:  receiver
 * @Version: 1.0.0
 * @Date: 2023/3/24
 * @Description:
 */

package tunnelx

import "github.com/alibaba/MongoShake/v2/oplog"

type Handler interface {
	Namespace() string
	Insert(oplogs *oplog.ParsedLog) (err error)
	Update(oplogs *oplog.ParsedLog) (err error)
	Delete(oplogs *oplog.ParsedLog) (err error)
}

type Routes map[string]Handler

var routes = make(Routes)

func RegistryRouter(handler Handler) {
	routes[handler.Namespace()] = handler
}
